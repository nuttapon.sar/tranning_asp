﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemeWebTraning.Models
{
    public class RegisterModel
    {
        public string regisNo { get; set; }
        public string organization { get; set; }

        public string firstName { get; set; }

        public string surName { get; set; }

        public string email { get; set; }

        public string confirmEmail { get; set; }

        public string mobileNo { get; set; }

        public string price { get; set; }

        public string payAmount { get; set; }

        public string payInDate { get; set; }

        public string payInTime { get; set; }

        public HttpPostedFileBase FilePicture { get; set; }

        public string UserImage { get; set; }

        public string search { get; set; }


    }
}