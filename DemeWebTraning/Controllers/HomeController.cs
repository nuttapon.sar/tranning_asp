﻿using DemeWebTraning.Models;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemeWebTraning.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Home()
        {
            return View();
        }

        public ActionResult Register(RegisterModel model)
        {
            if(model.FilePicture != null && model.FilePicture.ContentLength > 0)
            {
                var fileName = Path.GetFileName(model.FilePicture.FileName);
                var saveName = DateTime.Now.ToFileTime() + "_" + fileName;
                var path = Path.Combine(Server.MapPath("~/MiddleFiles/UserIMG"), saveName);
                model.UserImage = saveName;
                model.FilePicture.SaveAs(path);
            }
            else
            {
                model.UserImage = "";
            }
            return RedirectToAction("Home");
        }

        public JsonResult SearchUser(RegisterModel model)
        {
            model.mobileNo = "0955740669";
            return Json(model);
        }
        
    }
}