﻿using DemeWebTraning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemeWebTraning.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Authentication(string tag)
        {
            ViewBag.Tag = tag;
            ViewBag.Status = TempData["Status"];
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UserLogin(LoginModel user)
        {
            
            var userLogin = user;
            if(userLogin.username.Equals("admin") && userLogin.password.Equals("1234"))
            {
                TempData["Status"] = "Y";
                return RedirectToAction("Home", "Home");
            }
            else
            {
                TempData["Status"] = "N";
                return RedirectToAction("Authentication");

            }
        }


    }
}